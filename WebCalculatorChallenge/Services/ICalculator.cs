﻿namespace WebCalculatorChallenge.Services
{
    public interface ICalculator
    {
        string CalculateResult(string query);
    }
}