﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebCalculatorChallenge.Services
{
    public class Calculator : ICalculator
    {
        public string CalculateResult(string query)
        {
            var result = MultiplyAndDivide(SplitQueryIntoArray(query));

            return AddAndSubtract(result).FirstOrDefault();
        }

        private string[] SplitQueryIntoArray(string query)
        {
            query = query.Replace(" ", "").TrimEnd('+').TrimEnd('-').TrimEnd('*').TrimEnd('/');

            var stringBuilder = new StringBuilder(query);

            var previousIndexWasAnOperand = false; // to allow negative numbers
            for (int i = 0; i < stringBuilder.Length; i++)
            {
                if (stringBuilder[i] == '*' || stringBuilder[i] == '/' || stringBuilder[i] == '+' || stringBuilder[i] == '-')
                {
                    if (!previousIndexWasAnOperand && i != 0)
                    {
                        stringBuilder.Insert(i, " ").Insert(i + 2, " ");
                        i = i + 2;
                        previousIndexWasAnOperand = true;
                    }
                }
                else
                {
                    previousIndexWasAnOperand = false;
                }
            }

            return stringBuilder.ToString().Split(' ');
        }

        private string[] AddAndSubtract(string[] operations)
        {
            if (operations.Length < 2)
            {
                return operations;
            }

            var updatedOperations = new List<string>();

            for (int i = 0; i < operations.Length; i++)
            {
                if (operations[i] == "+")
                {
                    var result = Decimal.Parse(operations[i - 1]) + Decimal.Parse(operations[i + 1]);

                    for (int j = 0; j < operations.Length; j++)
                    {
                        if (j != i - 1 && j != i && j != i + 1)
                        {
                            updatedOperations.Add(operations[j]);
                        }
                        else if (j == i)
                        {
                            updatedOperations.Add(result.ToString());
                        }
                    }
                }
                else if (operations[i] == "-")
                {
                    var result = Decimal.Parse(operations[i - 1]) - Decimal.Parse(operations[i + 1]);

                    for (int j = 0; j < operations.Length; j++)
                    {
                        if (j != i - 1 && j != i && j != i + 1)
                        {
                            updatedOperations.Add(operations[j]);
                        }
                        else if (j == i)
                        {
                            updatedOperations.Add(result.ToString());
                        }
                    }
                }

                if (updatedOperations.Any())
                {
                    return AddAndSubtract(updatedOperations.ToArray());
                }
            }

            return operations;
        }


        private string[] MultiplyAndDivide(string[] operations)
        {
            if (operations.Length < 2)
            {
                return operations;
            }

            var updatedOperations = new List<string>();

            for (int i = 0; i < operations.Length; i++)
            {
                if (operations[i] == "*")
                {
                    var first = operations[i - 1];
                    var second = operations[i + 1];
                    var result = Decimal.Parse(first) * Decimal.Parse(second);

                    for (int j = 0; j < operations.Length; j++)
                    {
                        if (j != i - 1 && j != i && j != i + 1)
                        {
                            updatedOperations.Add(operations[j]);
                        }
                        else if (j == i)
                        {
                            updatedOperations.Add(result.ToString());
                        }
                    }
                }
                else if (operations[i] == "/")
                {
                    var first = operations[i - 1];
                    var second = operations[i + 1];
                    var result = Decimal.Parse(first) / Decimal.Parse(second);

                    for (int j = 0; j < operations.Length; j++)
                    {
                        if (j != i - 1 && j != i && j != i + 1)
                        {
                            updatedOperations.Add(operations[j]);
                        }
                        else if (j == i)
                        {
                            updatedOperations.Add(result.ToString());
                        }
                    }
                }

                if (updatedOperations.Any())
                {
                    return MultiplyAndDivide(updatedOperations.ToArray());
                }
            }

            return operations;
        }
    }
}