﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


$('#query').keyup(function () {


    $.ajax({
        type: "POST",
        url: `api/Calculate`,
      
        data: JSON.stringify($('#query').val()),  
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            //console.log(result);

            $("#query-result").text(result).css({ 'border-color': 'green' });  
            $("#query").css({ 'border-color' : 'green' });  
        },
        error: function (response) {
            $("#query-result").css({ 'border-color': 'red' });  
            $("#query").css({ 'border-color': 'red' });  
        }
      
    });
});