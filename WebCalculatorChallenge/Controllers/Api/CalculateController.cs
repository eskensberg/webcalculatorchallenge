﻿using System;
using Microsoft.AspNetCore.Mvc;
using WebCalculatorChallenge.Services;

namespace WebCalculatorChallenge.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalculateController : ControllerBase
    {
        private readonly ICalculator _calculator;

        public CalculateController(ICalculator calculator)
        {
            _calculator = calculator;
        }

        [HttpPost]
        public IActionResult Post([FromBody] string query)
        {
            try
            {
                return Ok(_calculator.CalculateResult(query));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
