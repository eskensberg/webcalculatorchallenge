using FluentAssertions;
using WebCalculatorChallenge.Services;
using Xunit;
using Xunit.Abstractions;

namespace WebCalculatorChallenge.Tests
{
    public class CalculatorTests
    {
        private readonly ITestOutputHelper _writer;

        public CalculatorTests(ITestOutputHelper writer)
        {
            _writer = writer;
        }

        [Fact]
        public void CalculateResult()
        {
            var query = "1 +2- 3*4/ 5-7- 8";

            var calc = new Calculator();

            var result = calc.CalculateResult(query);

            result.Should().Be("-14.4");

            _writer.WriteLine(result);
        }

        [Fact]
        public void CalculateResult_2()
        {
            var query = "-10 +20- -30*-40/ 50- -70- 80/";

            var calc = new Calculator();

            var result = calc.CalculateResult(query);

            result.Should().Be("-24");

            _writer.WriteLine(result);
        }
    }
}
